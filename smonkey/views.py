import requests
from flask import make_response, render_template, request, session

from smonkey import app


@app.route('/', methods=['GET', 'POST'])
@app.route('/index', methods=['GET', ' POST'])
def index():
    response = make_response(render_template('survey.html'))
    response.headers['Access-Control-Allow-Origin'] = '*'

    if request.method == 'POST':
        s = requests.Session()
        url = 'https://www.surveymonkey.com/r/GGWY326'
        r = s.post(url, data=request.form)
        for cookie in s.cookies:
            response.set_cookie(cookie.name, cookie.value)
            session[cookie.name] = cookie.value
        response = make_response('redirect')
    return response


@app.route('/survey')
def survey():
    s = requests.Session()
    url = 'https://www.surveymonkey.com/r/GGWY326'
    r = s.get(url, cookies=dict(session))
    response = make_response(r.text)
    return response
